package com.matfyz.castlewars;

/**
 * FORTIFIED - effective against all dmgTypes except of SIEGE
 * HEAVY - uneffective against MAGIC dmgType
 * MEDIUM - uneffective against NORMAL
 * LIGHT -uneffective against PIERCE
 * NONE - uneffective against PIERCE
 * HERO - effective against all dmgTypes
 *
 * TODO: doplniť/upraviť armorTypes
 */
public enum ArmorType {
	FORTIFIED, HEAVY, MEDIUM, LIGHT, NONE, HERO
}