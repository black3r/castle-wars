package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Contains methods for drawing and positioning object in gameGrid. Will contain
 * support for animated objects.
 *
 * Extended by: Destroyable(hyperlink) Projectile(hyperlink)
 */
public abstract class Drawable {
	// TODO: animation support
	int tileSize;
	int x;
	int y;
	Texture sprite;
	final CastleWarsGame game;
	int moveSpeed;
	boolean moving = false;
	int fromx;
	int fromy;
	int dx;
	int dy;
	float lastDelta = 0;

	public Drawable() {
		this.game = null;
	}

	public Drawable(int x, int y, int tileSize, Texture sprite,
			CastleWarsGame game) {
		this.x = x;
		this.y = y;
		this.tileSize = tileSize;
		this.sprite = sprite;
		this.game = game;
	}

	public int realX() {
		if (moving) {
			float delta =
					game.state.time * 1000 / game.constants.getStepDuration();
			if (delta >= lastDelta) {
				lastDelta = delta;
				float mx = dx * delta;
				float posx = (fromx + mx) * tileSize;
				return (int) posx;
			} else {
				lastDelta = delta;
				moving = false;
				return fromx + dx;
			}
		}
		return x * tileSize;
	}

	public int realY() {
		if (moving) {
			float delta =
					game.state.time * 1000 / game.constants.getStepDuration();
			if (delta >= lastDelta) {
				lastDelta = delta;
				float my = dy * delta;
				float posy = (fromy + my) * tileSize;
				return (int) posy;
			} else {
				lastDelta = delta;
				moving = false;
				return fromy + dy;
			}
		}
		return y * tileSize;
	}

	public void draw(Viewport vp, SpriteBatch batch) {
		if (inViewport(vp)) {
			batch.draw(sprite, realX() - vp.x, realY() - vp.y);
		}
	}

	public boolean inViewport(Viewport vp) {
		if (realX() >= vp.x && realX() + sprite.getWidth() <= (vp.x + vp.width)) {
			if (realY() >= vp.y
					&& realY() + sprite.getHeight() <= (vp.y + vp.height)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Moves this drawable to coordinates <x, y> in tileset.
	 * @param x
	 * @param y
	 */
	public void moveTo(int x, int y) {
		//System.out.println("DRAWABLE: MoveTo()");
		if (!moving) {
			moving = true;
			lastDelta = 0;
			this.fromx = this.x;
			this.fromy = this.y;
			this.dx = x - this.x;
			this.dy = y - this.y;
		} else {
			this.dx += (x - this.x);
			this.dy += (y - this.y);
		}
		if (game.state.moveDrawable(x, y, this)) {
			this.x = x;
			this.y = y;
		}
	}

	/**
	 * Moves this drawable dx tiles to the right and dy tiles up.
	 * @param dx
	 * @param dy
	 */
	public void move(int dx, int dy) {
		//System.out.println("DRAWABLE: move()");
		moveTo(x + dx, y + dy);
	}

	/**
	 * Takes a step towards target location. Returns true if already there,
	 * otherwise returns false.
	 *
	 * @param tx
	 * @param ty
	 * @return
	 */
	public boolean stepTowards(int tx, int ty) {
		//System.out.println("DRAWABLE: stepTowards()");
		if (x == tx && y == ty) {
			return true;
		} else {
			int dx = tx - x;
			int dy = ty - y;
			int adx = Math.abs(dx);
			int ady = Math.abs(dy);
			// Move zig-zaggy ;)
			if (adx > ady) {
				move(dx / adx, 0);
			} else {
				move(0, dy / ady);
			}
			return (x == tx && y == ty);
		}
	}

	/**
	 * Tries to move towards target location. Takes moveSpeed steps. Returns
	 * true if already there, otherwise returns false.
	 *
	 * @param tx
	 * @param ty
	 * @return
	 */
	public boolean moveTowards(int tx, int ty) {
		//System.out.println("DRAWABLE: moveTowards");
		boolean result = false;
		for (int i = 0; i < moveSpeed; i++) {
			result = result || stepTowards(tx, ty);
		}
		return result;
	}

	/**
	 * Tries to move towards target.
	 *
	 * @param enemy
	 */
	public boolean moveTowards(Destroyable enemy) {
		//System.out.println("DRAWABLE: moveTowards");

		return moveTowards(enemy.x, enemy.y);
	}
}
