package com.matfyz.castlewars;

/**
 * Enumerats types of damage used in game;
 * NORMAL - not very effective against fortified armor
 * PIERCE - effective againts lightly armored(or unamored) units
 * MAGIC - effective against heavy armor units
 * SIEGE - effective against fortified armor(buildings)
 * HERO - effective :)
 * NONE - No damage
 *
 * TODO: doplniť/upraviť dmgTypes
 */
public enum DamageType {
	NORMAL, PIERCE, MAGIC, SIEGE, HERO, NONE
}
