package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.matfyz.castlewars.GameState.Route;

/**
 * Class representing one Minion in the field. Has methods for carrying out its
 * step (attack if enemies in range, else check if enemies in visibility range,
 * if yes, move tovards them, if no, move to the next waypoint)
 */
public class Minion extends Destroyable {
	Route moveStrategy;
	Array<Integer> routX;
	Array<Integer> routY;
	int waypointNumber = 0;

	public Minion(Player owner, int moveStrategyNumber, int moveSpeed, int x,
			int y, int tileSize, Texture sprite, int hp, int dmg,
			int sightRange, int weaponRange, CastleWarsGame game) {
		super(owner, x, y, tileSize, sprite, hp, dmg, game);
		this.moveSpeed = moveSpeed;
		moveStrategy = game.state.routes.get(moveStrategyNumber);
		routX = moveStrategy.routeX;
		routY = moveStrategy.routeY;
		this.sightRange = sightRange;
		this.weaponRange = weaponRange;
		game.state.spawnMinion(x, y, this);
	}

	/**
	 * moveUnit to next waypoint specified in gameConstants
	 */
	private boolean moveToNextWaypoint() {
		if (routX.size > waypointNumber && routY.size > waypointNumber) {
			boolean moveT = moveTowards(routX.get(waypointNumber),
					routY.get(waypointNumber));
			if (moveT && waypointNumber != routX.size) {
				waypointNumber++;
			}
		}
		return false;
	}

	/**
	 * Unit preform its next step attack if there is enemy in range or move to
	 * next waypoint specified by its Route.
	 */
	public void act() {
		// find closest in range. If enemy found in sight-range but not in attack-range, then moveTowards(enemy)
		// if enemy in attack-range, then attack enemy.
		moving = false;
		// TODO testnut toto
		if (!isDestroyed) {
			Destroyable target =
					game.state.findNearestEnemyCloserThan(x, y, owner,
							sightRange);
			if (target == null) {
				moveToNextWaypoint();
			} else {
				int range = game.state.distanceFrom(x, y, target);
				if (range <= weaponRange) {
					attack(target);
				} else {
					moveTowards(target);
				}
			}
		}
	}
}
