package com.matfyz.castlewars;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Contains player state information, like array of minions,
 * buildings, upgrade states, and links to player factories.
 */
public class Player {
	public int id;
	public String name;
	public String race;
	public ArrayList<Minion> minions;
	public ArrayList<Building> buildings;
	public int goldCount;
	public int killCount;
	public Castle castle;

	public HashMap<String, Integer> buildingLevels =
			new HashMap<String, Integer>();

	public Player(String name, String race) {
		this.name = name;
		this.race = race;
		minions = new ArrayList<Minion>();
		buildings = new ArrayList<Building>();
		this.goldCount = 0;
		this.killCount = 0;
	}

	/**
	 * Do not use this constructor just for kryonet !!
	 */
	public Player() {
		this.goldCount = 0;
		this.killCount = 0;
		minions = new ArrayList<Minion>();
		buildings = new ArrayList<Building>();
	}

	public void upgradeBuilding(String type) {
		System.out.println("UPGRADEE");
		buildingLevels.put(type, Integer.valueOf(buildingLevels.get(type))
				.intValue() + 1);
		this.goldCount -= 200;
	}

	public int getBuildingLevel(String type) {
		return buildingLevels.get(type).intValue();
	}

	@Override
	public String toString() {
		return name + " " + race;
	}
}
