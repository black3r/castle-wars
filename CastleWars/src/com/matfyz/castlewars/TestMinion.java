package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;

public class TestMinion extends Minion {

	public TestMinion(Player owner, int x, int y, int moveStrategyNumber,
			CastleWarsGame game) {
		super(owner, moveStrategyNumber, 1, x, y, 32,
				(Texture) game.assetManager.get("sprites/minion.png"), 100, 20,
				20, 5, game);
	}
}
