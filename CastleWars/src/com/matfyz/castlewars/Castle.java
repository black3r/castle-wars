package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;

/**
 * Main building of each player, contains methods for creating hero, special
 * methods for notifying players about being attacked. After being destroyed
 * game ends for castle owner.
 *
 * Extends: Building(hyperlink)
 */

// TODO: dmg a range castle-u, idealne aj do constants.cfg
public class Castle extends Building {
	public Castle(Player owner, int x, int y, int tileSize, Texture sprite,
			int hp, int dmg,
			CastleWarsGame game) {
		super(owner, x, y, tileSize, sprite, hp, dmg, game);
	}

	public Castle(Player owner, int x, int y, int tileSize, Texture sprite,
			int hp, int dmg, CastleWarsGame game, long buildStep) {
		super(owner, x, y, tileSize, sprite, hp, dmg, game, buildStep);
	}

	/**
	 * Performs one game step.
	 */
	@Override
	public void act() {
		// Try to find an enemy in sight range!
		Destroyable target =
				game.state.findNearestEnemyCloserThan(x, y, owner, sightRange);
		// in case anyone wonders: sightRange might be shorter than weaponRange sometimes in the future
		if (target == null) {
		} else {
			int range = game.state.distanceFrom(x, y, target);
			if (range <= weaponRange) {
				attack(target);
			}
		}
	}
}



