package com.matfyz.castlewars;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Contains object locations on map, is capable of giving list of objects in
 * a given area, etc...
 *
 * Should be interfaced from GameState
 */
public class GameGrid {

	private Array<Array<Tile>> tileset = new Array<Array<Tile>>();
	// Grid dimensions are in tile counts
	private int width, height;
	public int tilesize;
	public Texture backgroundTexture;
	public TextureRegion backgroundImage;

	public GameGrid(int width, int height, int tilesize,
			Texture backgroundTexture) {
		this.width = width;
		this.height = height;
		this.tilesize = tilesize;
		this.backgroundTexture = backgroundTexture;
		this.backgroundImage =
				new TextureRegion(backgroundTexture, 0, 0, width * tilesize,
						height * tilesize);

		for (int i = 0; i < width; i++) {
			Array<Tile> tilerow = new Array<Tile>();
			for (int j = 0; j < height; j++) {
				tilerow.add(new Tile(i, j));
			}
			tileset.add(tilerow);
		}
	}

	/**
	 * Adds specified inhabitant to the specified coordinates if possible.
	 * Otherwise, return false.
	 * @param x
	 * @param y
	 * @param what
	 * @return
	 */
	public boolean add(int x, int y, Drawable what) {
		return tileset.get(x).get(y).addInhabitant(what);
	}

	/**
	 * Returns array of neighbouring tiles for the specified tile.
	 * Neighbouring tiles are those which share one side.
	 * @param t
	 * @return
	 */
	private Array<Tile> getNeighbours(Tile t) {
		int dx[] = { -1, 0, 1, 0 };
		int dy[] = { 0, 1, 0, -1 };
		Array<Tile> result = new Array<Tile>();
		for (int i = 0; i < 4; i++) {
			int x = t.x + dx[i];
			int y = t.y + dy[i];
			if (x >= 0 && y >= 0 && x < width && y < height) {
				result.add(tileset.get(x).get(y));
			}
		}
		return result;
	}

	/**
	 * Finds nearest enemy unit from position [x,y]. Enemy is anyone other
	 * than currentPlayer
	 * TODO pripadne spojenectva
	 * @param x
	 * @param y
	 * @param currentPlayer
	 * @return
	 */
	public Destroyable findNearestEnemy(int x, int y, Player currentPlayer) {
		Queue<Tile> queue = new LinkedList<Tile>();
		HashSet<Tile> visited = new HashSet<Tile>();
		queue.add(tileset.get(x).get(y));
		visited.add(tileset.get(x).get(y));
		while (!queue.isEmpty()) {
			Tile t = queue.poll();
			Destroyable target = t.getTarget();
			if (target != null && target.owner != currentPlayer) {
				return target;
			} else {
				for (Tile neighbour : getNeighbours(t)) {
					if (!visited.contains(neighbour)) {
						visited.add(neighbour);
						queue.add(neighbour);
					}
				}
			}
		}
		return null;
	}

	// TODO mergnut s findNearestEnemyCloserThan (napr. volat to s rozumnym nekonecnom)

	public Destroyable findNearestEnemyCloserThan(int x, int y,
			Player currentPlayer, int maxDistance) {
		Queue<Tile> queue = new LinkedList<Tile>();
		HashSet<Tile> visited = new HashSet<Tile>();
		queue.add(tileset.get(x).get(y));
		visited.add(tileset.get(x).get(y));
		while (!queue.isEmpty()) {
			Tile t = queue.poll();
			Destroyable target = t.getTarget();
			if (target != null && target.owner != currentPlayer) {
				return target;
			} else {
				for (Tile neighbour : getNeighbours(t)) {
					if (!visited.contains(neighbour)) {
						// TODO: Spravit funkciu game.state.distanceBetween(int x1, y1, x2, y2) a pouzit tu
						// (mozno niekedy vymenime vzdialenost z manhattanovskej na 8-smernu)
						if (Math.abs(neighbour.x - x)
								+ Math.abs(neighbour.y - y) <= maxDistance) {
							visited.add(neighbour);
							queue.add(neighbour);
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Places the minion on or near the specified tile. Uses BFS to
	 * find nearest available tile.
	 * @param x
	 * @param y
	 * @param what
	 */
	public void forceAdd(int x, int y, Drawable what) {
		// correct if something wants to be spawn outside screen borders.
		if (x < 0) {
			x = 0;
		}
		if (y < 0) {
			y = 0;
		}
		if (x >= width) {
			x = width - 1;
		}
		if (y >= height) {
			y = height - 1;
		}
		Queue<Tile> queue = new LinkedList<Tile>();
		HashSet<Tile> visited = new HashSet<Tile>();
		queue.add(tileset.get(x).get(y));
		visited.add(tileset.get(x).get(y));
		while (!queue.isEmpty()) {
			Tile t = queue.poll();
			if (t.addInhabitant(what)) {
				break;
			} else {
				for (Tile neighbour : getNeighbours(t)) {
					if (!visited.contains(neighbour)) {
						visited.add(neighbour);
						queue.add(neighbour);
					}
				}
			}
		}
	}

	/**
	 * Checks if specified inhabitant can be moved to specified location.
	 * @param x
	 * @param y
	 * @param what
	 * @return
	 */
	public boolean canBeMovedTo(int x, int y, Drawable what) {
		return tileset.get(x).get(y).pollInhabitant(what);
	}

	/**
	 * Removes the specified inhabitant from grid.
	 * @param what
	 */
	public void remove(Drawable what) {
		tileset.get(what.x).get(what.y).removeInhabitant(what);
	}

	/**
	 * Tries to move the specified inhabitant to specified location.
	 * Returns true if the move is successful, otherwise returns false.
	 * @param x
	 * @param y
	 * @param what
	 * @return
	 */
	public boolean moveTo(int x, int y, Drawable what) {
		if (!canBeMovedTo(x, y, what)) {
			return false;
		}
		remove(what);
		return add(x, y, what);
	}

	/**
	 * Moves the specified inhabitant to or near the specified location.
	 * @param x
	 * @param y
	 * @param what
	 */
	public void forceMoveTo(int x, int y, Drawable what) {
		remove(what);
		forceAdd(x, y, what);
	}

	public void drawBackground(Viewport vp, SpriteBatch batch) {
		TextureRegion region =
				new TextureRegion(backgroundTexture, vp.x, vp.y, vp.width,
						vp.height);
		batch.draw(region, 0, 0);
	}

	public Barracks getBuildingFromCoords(int tx, int ty) {
		if (tx < 0 || tx >= 25 || ty < 0 || ty >= 15) {
			return null;
		}
		LinkedList<Drawable> inhabitants = this.tileset.get(tx).get(ty).inhabitants;
		for (Drawable d : inhabitants) {
			if (d instanceof Barracks) {
				return  (Barracks) d;
			}
		}
		return null;
	}
}
