package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;

/**
 * Class adds methods for destroying building/unit , hp, etc..
 *
 * Implements: Drawable(hyperlink)
 *
 * Extended by: Building(hyperlink) Minion(hyperlink) ??
 */
public class Destroyable extends Drawable {
	Player owner;
	public int hp;
	Texture destroyed;
	boolean isDestroyed;
	int dmg;
	int sightRange;
	int weaponRange;
	DamageType dmgType;
	ArmorType armorType;
	boolean splashDmg;

	public Destroyable() {
		dmgType = DamageType.NONE;
		armorType = ArmorType.NONE;
	}

	public Player getOwner() {
		return this.owner;
	}

	public Destroyable(Player owner, int x, int y, int tileSize,
			Texture sprite, int hp,
			int dmg, CastleWarsGame game) {
		// TODO: Prerobit znovu konstruktory, x a y nastavi grid, ked
		// je destroyable v nom zaregistrovany
		super(x, y, tileSize, sprite, game);
		this.owner = owner;
		dmgType = DamageType.NONE;
		armorType = ArmorType.NONE;
		this.hp = hp;
		this.dmg = dmg;

		// TODO: Bud pridat do tohto konstruktora vlastnika, alebo spravit dalsi
		// konstruktor, ktory nastavuje vlastnika, alebo aspon metody
		// setOwner(), getOwner(), idealne metody a novy konstruktor
	}

	public Destroyable(DamageType dmgType, ArmorType armorType) {
		this.dmgType = dmgType;
		this.armorType = armorType;
	}

	/**
	 * Implements logic of being under attack
	 * @param dmg Base DMG of attacker
	 * @param dmgType DMG type of attack
	 * @param attacker Player that attacked you
	 */
	public void wasAttacked(int dmg, DamageType dmgType, Player attacker) {
		int dmgPoints = dmg * dmgMultiplier(dmgType, armorType);
		hp -= dmgPoints;
		if (hp <= 0) {
			setDestroyedBy(attacker);
		}
	}

	/**
	 * Implements logic of being under attack.
	 *
	 * @param attacker
	 *            - destroyable that attacked this object
	 */
	public void wasAttacked(Destroyable attacker) {
		//System.out.println("Was attacked by melee");
		wasAttacked(attacker.dmg, attacker.dmgType, attacker.owner);
	}

	/**
	 * Implements logic of being attacked by a projectile.
	 *
	 * @param attacker
	 * 			- projectile that attacked this object
	 */
	public void wasAttacked(Projectile attacker) {
		//System.out.println("Was attacked by projectile");
		wasAttacked(attacker.dmg, attacker.dmgType, attacker.owner);
	}

	/**
	 * Used when computing how many health points should be taken , based on
	 * dmgType of attacker and armorType of attacked.
	 *
	 * @param dmgType
	 * @param armorType
	 * @return Percentage of damage to be taken.
	 */
	private int dmgMultiplier(DamageType dmgType, ArmorType armorType) {
		// TODO compute damage multiplier
		return 1;
	}

	/**
	 * Implements logic of attacking.
	 *
	 * @param attacked
	 *            - object under attack
	 */
	public void attack(Destroyable attacked) {
		if (weaponRange == 1) {
			attacked.wasAttacked(this);
		} else {
			new Projectile(this, attacked);
		}
	}

	/**
	 * Re-draw this object with destroyed sprite. Destroyed unit should remain
	 * dead for 1 gameStep //???? then should fadeOut //????
	 */
	public void setDestroyed() {
		sprite = destroyed;
		isDestroyed = true;
	}

	public void setDestroyedBy(Player attacker) {
		setDestroyed();
		attacker.killCount++;
		attacker.goldCount += 20; // TODO: pridat sem skutocnu hodnotu ktoru ma hrac dostat
		if (this instanceof Minion) {
			game.state.flagRemoved((Minion) this);
		} else if (this instanceof Building) {
			game.state.flagRemoved((Building) this);
		}
	}

}
