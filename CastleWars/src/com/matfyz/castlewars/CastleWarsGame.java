package com.matfyz.castlewars;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.matfyz.castlewars.screens.SplashScreen;


public class CastleWarsGame extends Game {
	public GameConstants constants;
	public GameConfig config;
	public GameState state;
	public AssetManager assetManager;
	public OrthographicCamera camera;

	@Override
	public void create() {
		assetManager = new AssetManager();
		state = null;
		config = new GameConfig();
		constants = new GameConstants();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
		setScreen(new SplashScreen(this));
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void render() {
		super.render();
	}


	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}
}
