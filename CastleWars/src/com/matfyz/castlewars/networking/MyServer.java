package com.matfyz.castlewars.networking;

import java.io.IOException;
import java.net.BindException;
import java.util.ArrayDeque;
import java.util.ArrayList;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.matfyz.castlewars.ArmorType;
import com.matfyz.castlewars.Barracks;
import com.matfyz.castlewars.Building;
import com.matfyz.castlewars.Castle;
import com.matfyz.castlewars.DamageType;
import com.matfyz.castlewars.Player;
import com.matfyz.castlewars.Tower;

/**
 * Server class for testing network interface
 */
public class MyServer {

	public ArrayList<Player> players;
	public ArrayList<Connection> connections;
	public Server server;
	public String gameType;
	public boolean isServer = false;
	public ArrayDeque<NewBuildingPacket> toBuild;
	private static MyServer myServer;
	public ArrayDeque<ChangeRoutePacket> routesToChange;
	public ArrayDeque<BarracksUpgradePacket> barracksToUpgrade;
	public boolean readyToMove = true;
	public int clientReadyToMove;
	public boolean raceChanged = false;

	public static MyServer getInstance(String gameType) {
		if (myServer == null) {
			myServer = new MyServer(gameType);
		} else if (myServer.gameType != null && !myServer.gameType.equals(gameType) && myServer.isServer) {
			myServer.gameType = gameType;
		}
		return myServer;
	}

	public void closeServer() {
		server.stop();
		myServer = null;
	}

	private MyServer(final String gameType) {
		server = new Server();
		clientReadyToMove = 0;
		this.gameType = gameType;
		players = new ArrayList<Player>();
		connections = new ArrayList<Connection>();
		toBuild = new ArrayDeque<NewBuildingPacket>();
		routesToChange = new ArrayDeque<ChangeRoutePacket>();
		barracksToUpgrade = new ArrayDeque<BarracksUpgradePacket>();

		server.start();
		try {
			// server.bind(tcpPort, udpPort);
			server.bind(51998, 51999);
		} catch (IOException e) {
			if (e instanceof BindException == false) {
				e.printStackTrace();
			}
		}
		server.addListener(new Listener() {
			@Override
			public void received(Connection connection, Object object) {
				if (object instanceof ConnectionRequestPacket) {
					ConnectionRequestPacket request = (ConnectionRequestPacket) object;
					ConnectionRespondPacket respond = new ConnectionRespondPacket();
					if (gameType.equals("1x1") && players.size() < 2
							|| gameType.equals("2x2") && players.size() < 4) {
						respond.allowedToJoin = true;
						request.player.id = players.size();
						respond.id = players.size();
						players.add(request.player);
						respond.connectedPlayers = players;
						//System.out.println("server connectionRequest");
					} else {
						respond.allowedToJoin = false;
					}
					connection.sendTCP(respond);
					if (respond.allowedToJoin) {
						tellOthers(connection, object);
						connections.add(connection);
					}
				} else if (object instanceof NewBuildingPacket) {
					tellOthers(connection, object);
					toBuild.add((NewBuildingPacket) object);
					//System.out.println("server NewBuildingPacket");
				} else if (object instanceof ChangeRoutePacket) {
					tellOthers(connection, object);
					routesToChange.add((ChangeRoutePacket) object);
				} else if (object instanceof RaceChangedPacket) {
					tellOthers(connection, object);
					RaceChangedPacket paket = (RaceChangedPacket) object;
					players.get(paket.playerId).race = paket.race;
					raceChanged = true;
				} else if (object instanceof BarracksUpgradePacket) {
					tellOthers(connection, object);
					barracksToUpgrade.add((BarracksUpgradePacket) object);
				}
			}
		});

		// Objects must be registred so you can send them
		Kryo kryo = server.getKryo();
		kryo.register(Player.class);
		kryo.register(ArrayList.class);
		kryo.register(ConnectionRequestPacket.class);
		kryo.register(ConnectionRespondPacket.class);
		kryo.register(StartGamePacket.class);
		kryo.register(Building.class);
		kryo.register(NewBuildingPacket.class);
		kryo.register(Barracks.class);
		kryo.register(Tower.class);
		kryo.register(ArmorType.class);
		kryo.register(DamageType.class);
		kryo.register(java.lang.Class.class);
		kryo.register(com.matfyz.castlewars.TestTower.class);
		kryo.register(ChangeRoutePacket.class);
		kryo.register(RaceChangedPacket.class);
		kryo.register(java.util.HashMap.class);
		kryo.register(BarracksUpgradePacket.class);
		kryo.register(Castle.class);
	}

	private void tellOthers(Connection connection, Object object) {
		for (int i = 0; i < connections.size(); i++) {
			if (connection != connections.get(i)) {
				connection.sendTCP(object);
			}
		}
	}
}
