package com.matfyz.castlewars.networking;

import com.matfyz.castlewars.Barracks;
import com.matfyz.castlewars.CastleWarsGame;

public class ChangeRoutePacket {
	public int playerId;
	public String barracksType;
	public long step;

	public ChangeRoutePacket(Barracks what, CastleWarsGame game) {
		this.playerId = what.getOwner().id;
		this.barracksType = what.minionType;
		this.step = game.state.stepCount + 2;
	}

	public ChangeRoutePacket() {

	}
}
