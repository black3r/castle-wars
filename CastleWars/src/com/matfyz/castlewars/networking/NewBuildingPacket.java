package com.matfyz.castlewars.networking;


public class NewBuildingPacket {
	public int playerId;
	public int x, y;
	public String primaryType, secondaryType;
	public long step;
}
