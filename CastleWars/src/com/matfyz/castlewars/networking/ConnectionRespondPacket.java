package com.matfyz.castlewars.networking;



import java.util.ArrayList;

import com.matfyz.castlewars.Player;

public class ConnectionRespondPacket {
	int id;
	boolean allowedToJoin;
	ArrayList<Player> connectedPlayers;
}
