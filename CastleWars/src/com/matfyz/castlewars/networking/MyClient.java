package com.matfyz.castlewars.networking;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.matfyz.castlewars.ArmorType;
import com.matfyz.castlewars.Barracks;
import com.matfyz.castlewars.Building;
import com.matfyz.castlewars.Castle;
import com.matfyz.castlewars.DamageType;
import com.matfyz.castlewars.Player;
import com.matfyz.castlewars.Tower;

/**
 * Client class for testing network interface
 */
public class MyClient {
	public Client client;
	public boolean connected = false;
	public ArrayList<Player> players;
	private static MyClient myClient;
	public boolean gameStarted = false;
	public ArrayDeque<NewBuildingPacket> toBuild;
	public int myId = -1;
	public ArrayDeque<ChangeRoutePacket> routesToChange;
	public ArrayDeque<BarracksUpgradePacket> barracksToUpgrade;
	public boolean readyToMove = true;
	public boolean raceChanged = false;

	public static MyClient getInstance() {
		if (myClient == null) {
			myClient = new MyClient();
		}
		return myClient;
	}

	public void closeClient() {
		client.stop();
		myClient = null;
	}

	private MyClient() {
		players = new ArrayList<Player>();
		toBuild = new ArrayDeque<NewBuildingPacket>();
		routesToChange = new ArrayDeque<ChangeRoutePacket>();
		barracksToUpgrade = new ArrayDeque<BarracksUpgradePacket>();
		client = new Client();
		client.start();
		client.addListener(new Listener() {
			@Override
			public void received(Connection connection, Object object) {
				if (object instanceof ConnectionRespondPacket) {
					ConnectionRespondPacket respond = (ConnectionRespondPacket) object;
					connected = respond.allowedToJoin;
					if (connected) {
						players = respond.connectedPlayers;
						if (myId == -1) {
							myId = respond.id;
						}
					}
					//System.out.println("client connectionRespond");
				} else if (object instanceof StartGamePacket) {
					gameStarted = true;
					//System.out.println("client StartGamePacket");
				} else if (object instanceof NewBuildingPacket) {
					toBuild.add((NewBuildingPacket) object);
					//System.out.println("client NewBuildingPacket");
				} else if (object instanceof ChangeRoutePacket) {
					routesToChange.add((ChangeRoutePacket) object);
				} else if (object instanceof RaceChangedPacket) {
					RaceChangedPacket paket = (RaceChangedPacket) object;
					players.get(paket.playerId).race = paket.race;
					raceChanged = true;
				} else if (object instanceof BarracksUpgradePacket) {
					barracksToUpgrade.add((BarracksUpgradePacket) object);
				}
			}
		});
		// Objects must be registered so you can send them
		Kryo kryo = client.getKryo();
		kryo.register(Player.class);
		kryo.register(ArrayList.class);
		kryo.register(ConnectionRequestPacket.class);
		kryo.register(ConnectionRespondPacket.class);
		kryo.register(StartGamePacket.class);
		kryo.register(Building.class);
		kryo.register(NewBuildingPacket.class);
		kryo.register(Barracks.class);
		kryo.register(Tower.class);
		kryo.register(ArmorType.class);
		kryo.register(DamageType.class);
		kryo.register(java.lang.Class.class);
		kryo.register(com.matfyz.castlewars.TestTower.class);
		kryo.register(ChangeRoutePacket.class);
		kryo.register(RaceChangedPacket.class);
		kryo.register(java.util.HashMap.class);
		kryo.register(BarracksUpgradePacket.class);
		kryo.register(Castle.class);
	}

	public void connectToHost(InetAddress host) {
		try {
			client.connect(5000, host, 51998, 51999);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<InetAddress> getPossibleHosts() {
		// TODO: checkovať aj iné porty zo setu portov/spraviť set portov
		List<InetAddress> hosts = client.discoverHosts(51999, 2000);
		return hosts;
	}
}
