package com.matfyz.castlewars;

import java.net.InetAddress;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.esotericsoftware.kryonet.Connection;
import com.matfyz.castlewars.networking.ConnectionRequestPacket;
import com.matfyz.castlewars.networking.MyClient;
import com.matfyz.castlewars.networking.MyServer;
import com.matfyz.castlewars.networking.RaceChangedPacket;
import com.matfyz.castlewars.screens.AbstractScreen;
import com.matfyz.castlewars.screens.MainMenuScreen;

public class UI {
	protected final CastleWarsGame game;
	public TextureAtlas buttonTextureAtlas;
	public Skin buttonSkin;
	public BitmapFont defaultFont;
	public TextButtonStyle textButtonStyle;

	public TextFieldStyle textFieldStyle;
	public TextureAtlas uiskinAtlas;
	public Skin uiSkin, skin;
	public ListStyle listStyle;
	public SelectBoxStyle selectBoxStyle;
	public ScrollPaneStyle scrollStyle;
	public WindowStyle windowStyle;
	public LabelStyle labelStyle;

	public UI(CastleWarsGame game) {
		this.game = game;
	}

	public void createUi() {

		uiskinAtlas = game.assetManager.get("ui/uiskin.atlas",
				TextureAtlas.class);
		uiSkin = new Skin(uiskinAtlas);
		skin = new Skin(Gdx.files.internal("ui/uiskin.json"));
		defaultFont = game.assetManager.get("ui/default.fnt", BitmapFont.class);

		textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = uiSkin.getDrawable("default-round");
		textButtonStyle.down = uiSkin.getDrawable("default-round-down");
		textButtonStyle.pressedOffsetX = 1;
		textButtonStyle.pressedOffsetY = -1;
		textButtonStyle.font = defaultFont;

		textFieldStyle = new TextFieldStyle();
		textFieldStyle.fontColor = Color.WHITE;
		textFieldStyle.cursor = uiSkin.getDrawable("cursor");
		textFieldStyle.background = uiSkin.getDrawable("textfield");
		textFieldStyle.font = defaultFont;

		windowStyle = new WindowStyle();
		windowStyle.titleFont = defaultFont;
		windowStyle.background = uiSkin.getDrawable("default-window");
		windowStyle.titleFontColor = Color.WHITE;

		labelStyle = new LabelStyle(defaultFont, Color.WHITE);

		listStyle = new ListStyle(defaultFont, Color.WHITE, Color.WHITE,
				uiSkin.getDrawable("default-rect-pad"));

		scrollStyle = new ScrollPaneStyle(uiSkin.getDrawable("default-rect"),
				uiSkin.getDrawable("default-scroll"),
				uiSkin.getDrawable("default-round-large"),
				uiSkin.getDrawable("default-scroll"),
				uiSkin.getDrawable("default-round-large"));

		selectBoxStyle = new SelectBoxStyle(defaultFont, Color.WHITE,
				uiSkin.getDrawable("selection"), scrollStyle, listStyle);

	}

	public void dispose() {
		buttonTextureAtlas.dispose();
		buttonSkin.dispose();
		defaultFont.dispose();
	}

	public TextButton createButton(String buttonText,
			TextButtonStyle textButtonStyle, float buttonHeight,
			ClickListener listener) {
		TextButton button = new TextButton(buttonText, textButtonStyle);
		button.pad(buttonHeight);
		button.addListener(listener);
		return button;
	}

	public TextField createTextfield(String text, float width, float height) {
		TextField tf = new TextField(text, skin);
		tf.setSize(width, height);
		return tf;
	}

	public Label createTextLabel(String text, int fontScale) {
		Label label = new Label(text, labelStyle);
		label.setFontScale(fontScale);
		return label;
	}

	public TextButton createBackButton() {
		return createButton("<- Back", textButtonStyle, 10,
				new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						game.setScreen(new MainMenuScreen(game));
					}
				});
	}

	public BitmapFont getDefaultFont() {
		return defaultFont;
	}

	public TextButton createScreenButton(String buttonText,
			TextButtonStyle textButtonStyle, float buttonHeight,
			final AbstractScreen screen) {
		return createButton(buttonText, textButtonStyle, buttonHeight,
				new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						if (screen == null) {
							Gdx.app.exit();
						} else {
							game.setScreen(screen);
						}
					}
				});
	}

	public class ConnectToHostDialog extends Dialog {

		ConnectionRequestPacket crp;

		public ConnectToHostDialog(String title, Skin skinn,
				ConnectionRequestPacket req, List<InetAddress> hosts) {
			super(title, skin);
			crp = req;
			this.text("Select host to join please");
			Table bt = this.getButtonTable();
			for (InetAddress host : hosts) {
				this.button(host.getHostAddress(), host);
				bt.row();
			}
			this.button("Back");
		}

		@Override
		protected void result(Object object) {
			super.result(object);
			if (object != null) {
				MyClient.getInstance().connectToHost((InetAddress) object);
				MyClient.getInstance().client.sendTCP(crp);
			}
		}

	}

	public ConnectToHostDialog createSelectHostDialog(String title,
			List<InetAddress> hosts, ConnectionRequestPacket crp) {
		return new ConnectToHostDialog(title, skin, crp, hosts);
	}

	public class ErrorDialog extends Dialog {

		public ErrorDialog(String title, Skin skin, String errorMessage) {
			super(title, skin);
			this.text(errorMessage);
			this.button("OK");
		}

	}

	public ErrorDialog createErrorDialog(String errorMessage) {
		return new ErrorDialog("Problem occured", buttonSkin, errorMessage);
	}

	public class ChooseRaceDialog extends Dialog {

		public ChooseRaceDialog(String title, Skin skin) {
			super(title, skin);
			this.text("Choose your race");
			this.button("Human", "Human");
			this.button("Orc", "Orc");
		}

		@Override
		protected void result(Object object) {
			super.result(object);
			MyServer server = MyServer.getInstance(null);
			MyClient client = MyClient.getInstance();
			RaceChangedPacket paket = new RaceChangedPacket();
			paket.race = object.toString();
			if (server.isServer) {
				paket.playerId = 0;
				server.players.get(0).race = object.toString();
				for (Connection c : server.connections) {
					c.sendTCP(paket);
				}
				server.raceChanged = true;
			}else {
				paket.playerId = client.myId;
				client.players.get(client.myId).race = object.toString();
				client.client.sendTCP(paket);
				client.raceChanged = true;
			}
		}

	}

	public com.badlogic.gdx.scenes.scene2d.ui.List createList(Object[] items, final Stage stage) {
		com.badlogic.gdx.scenes.scene2d.ui.List l = new com.badlogic.gdx.scenes.scene2d.ui.List(
				items, skin);
		l.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				new ChooseRaceDialog("Race Selection", skin).show(stage);
			}
		});
		return l;
	}
}
