package com.matfyz.castlewars;

/**
 * Interface conaing methods for communication between RemotePlayer(hyperlink)
 * and Communicator(hyperlink)
 * 
 * Implemented by:
 * Communicator(hyperlink)
 * RemotePlayer(hyperlink)
 */
public interface RemoteCommands {
	public void broadcastChange();
	public void broadcastPause();
}
