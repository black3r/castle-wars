package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Adds methods for path choosing, unit training(calls methods from factory)
 *
 * Extends: Building(hyperlink) Extended by: barracks extending Barracks
 */
public class Barracks extends Building {
	int moveStrategy;
	public String minionType;

	public Barracks(Player owner, int x, int y, CastleWarsGame game,
			String minionType) {
		super(owner, x, y, 32, game.assetManager.get(game.constants
				.getBarracks(owner.race).get(minionType).getString("sprite"),
				Texture.class), game.constants.getBarracks(owner.race)
				.get(minionType).getInt("hp"), 0, game);
		this.minionType = minionType;
		moveStrategy = owner.id * 2;
		this.data = game.constants.getUnits(owner.race).get(minionType);
	}

	public Barracks(Player owner, int x, int y, CastleWarsGame game,
			String minionType,
			long buildStep) {
		super(owner, x, y, 32, game.assetManager.get(game.constants
				.getBarracks(owner.race).get(minionType).getString("sprite"),
				Texture.class), game.constants.getBarracks(owner.race)
				.get(minionType).getInt("hp"), 0, game, buildStep);
		this.minionType = minionType;
		moveStrategy = owner.id * 2;
		this.data = game.constants.getUnits(owner.race).get(minionType);
	}

	int cooldown = 0;
	int cooldownMax = game.constants.getBuildingCooldown();
	JsonValue data;
	JsonValue leveled;

	/**
	 * Spawns the specified minion if cooldown passed;
	 */
	@Override
	public void act() {
		if (cooldown > 0) {
			cooldown--;
		} else {
			cooldown = cooldownMax;

			JsonValue leveled = data.get(owner.getBuildingLevel(minionType));
			new Minion(owner, moveStrategy, leveled.getInt("moveSpeed"), x, y,
					32, game.assetManager.get(leveled.getString("sprite"),
							Texture.class), leveled.getInt("hp"),
					leveled.getInt("dmg"), leveled.getInt("sightRange"),
					leveled.getInt("weaponRange"), game);
		}
	}

	public void changeRoute() {
		int playerOffset = owner.id * 2;
		moveStrategy =
				playerOffset + ((moveStrategy - playerOffset) == 1 ? 0 : 1);
	}

	public int getRouteNumber() {
		return moveStrategy - owner.id * 2 + 1;
	}

}
