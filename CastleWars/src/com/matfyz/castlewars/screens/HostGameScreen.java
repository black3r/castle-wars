package com.matfyz.castlewars.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.matfyz.castlewars.CastleWarsGame;

public class HostGameScreen extends AbstractScreen {

	Label heading, selectionPrompt, namePrompt;
	TextButton hostGame, backToMainMenu;
	SelectBox gameSelection;
	TextField textField;
	private Table table;
	Array<String> gameStyle;

	public HostGameScreen(final CastleWarsGame game) {
		super(game);
		textField = ui.createTextfield("Host", 20, 60);
		backToMainMenu =
				ui.createBackButton();
		heading = ui.createTextLabel("Host Game", 3);
		hostGame = ui.createButton("Create", ui.textButtonStyle, 20,new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x,
					float y) {
				int typeOfGame = gameSelection.getSelectionIndex();
				String hostName = textField.getText();
				game.setScreen(new LobbyScreen(game, gameStyle.get(typeOfGame), "server", hostName));
			}
		});

		gameStyle = new Array<String>();
		gameStyle.add("1x1");
		//gameStyle.add("2x2");
		selectionPrompt = ui.createTextLabel("Select game type:", 1);
		namePrompt = ui.createTextLabel("Please type your name", 1);
		gameSelection = new SelectBox(gameStyle.toArray(), ui.selectBoxStyle);
		table = createTable();
		stage.addActor(table);
	}

	private Table createTable() {
		Table table = new Table();
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		table.add(heading);
		table.getCell(heading).spaceBottom(50);
		table.row();
		Table t = new Table();
		t.size(300, 50);
		t.add(namePrompt).fill();
		t.row();
		t.add(textField).fill();
		t.row();
		t.add(selectionPrompt);
		t.row();
		t.add(gameSelection).fill();
		t.row();
		t.add(hostGame).fill();
		t.row();
		t.add(backToMainMenu).fill();
		table.add(t);
		table.debug();
		return table;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		table.setSize(width, height);
		table.invalidateHierarchy();
	}
}
