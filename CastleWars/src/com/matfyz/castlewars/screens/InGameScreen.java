
package com.matfyz.castlewars.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.matfyz.castlewars.Barracks;
import com.matfyz.castlewars.CastleWarsGame;
import com.matfyz.castlewars.GameState;
import com.matfyz.castlewars.Player;
import com.matfyz.castlewars.TestTower;
import com.matfyz.castlewars.Tower;
import com.matfyz.castlewars.Viewport;
import com.matfyz.castlewars.networking.BarracksUpgradePacket;
import com.matfyz.castlewars.networking.ChangeRoutePacket;
import com.matfyz.castlewars.networking.MyClient;
import com.matfyz.castlewars.networking.MyServer;
import com.matfyz.castlewars.networking.NewBuildingPacket;

/**
 * Class implementing common actions with showing/hiding the sidebar menu and
 * adding/removing buttons (or button sets) from it.
 */
class SideMenu extends Table {
	public boolean opened = false;

	public void toggle() {
		if (opened) {
			hide();
		} else {
			show();
		}
		opened = !opened;
	}

	public void show() {
		setBounds(600, 0, 200, 480);
	}

	public void hide() {
		setBounds(1000, 1000, 0, 0);
	}

	public void addButton(Button button) {
		add(button).fill();
		row();
	}
}

class BuildButton extends TextButton {
	final CastleWarsGame game;
	final InGameScreen screen;
	final Texture icon;
	final String minionType;

	public BuildButton(final String minionType, TextButtonStyle style,
			final CastleWarsGame game, final InGameScreen screen) {
		super(minionType
				+ "("
				+ game.constants.getBarracks(game.state.localPlayer.race)
						.get(minionType).getInt("price") + ")", style);
		this.minionType = minionType;
		this.game = game;
		this.screen = screen;
		this.icon =
				game.assetManager.get(
						game.constants.getBarracks(game.state.localPlayer.race)
								.get(minionType).getString("sprite"),
						Texture.class);
		pad(10);
		addListener(new DragListener() {
			@Override
			public void dragStart(InputEvent event, float x, float y,
					int pointer) {
				if (game.state.canHasCashForBarracks(game.state.localPlayer,
						minionType)) {
					screen.building = true;
					screen.toBeBuilt = icon;
				} else {
					System.out.println("NOOPE");
				}
			}

			@Override
			public void dragStop(InputEvent event, float x, float y, int pointer) {
				screen.building = false;
				screen.toBeBuilt = icon;
				int tx = (int) event.getStageX() / game.state.getTileSize();
				int ty = (int) event.getStageY() / game.state.getTileSize();
				//builder.build(tx, ty, game);
				if (game.state.canBuildBarracks(game.state.localPlayer,
						minionType, tx, ty)) {
					screen.informOthers(tx, ty, game.state.createBarracks(
							game.state.localPlayer, minionType, tx, ty));
				} else {
					System.out.println("NOOPE");
				}
			}
		});
	}
}

/**
 * Class representing game screen during gameplay.
 *
 * Implements: Screen(hyperlink)
 */
public class InGameScreen extends AbstractScreen {
	private Viewport vp;
	BitmapFont bitmapFont;
	private SpriteBatch batch;
	private TextButton buildMenuButton;
	private boolean menu_opened = false;
	private SideMenu menu, buildingMenu;
	public Texture toBeBuilt = null;
	public boolean building = false;
	private TextButton changeRoute;
	private int moveStrategy = 0;
	MyServer server = MyServer.getInstance(null);
	MyClient client = MyClient.getInstance();
	MyInputProcessor buildingClickProcessor = new MyInputProcessor();
	Barracks selectedBarracks;

	public InGameScreen(final CastleWarsGame game) {
		super(game);
		if (server.isServer) {
			// create GameState
			game.state = new GameState(game, 25, 15, 32,
					(Texture) game.assetManager.get("background.png"),
					server.players, server.players.get(0));
			game.state.createPlayers();
		} else {
			// create GameState
			game.state = new GameState(game, 25, 15, 32,
					(Texture) game.assetManager.get("background.png"),
					client.players, client.players.get(client.myId));
			game.state.createPlayers();
		}
		vp = new Viewport(0, 0, 800, 480);
		batch = new SpriteBatch();

		menu = new SideMenu();
		menu.hide();


		buildingMenu = new SideMenu();
		buildingMenu.hide();

		buildMenuButton = ui.createButton("Buildings", ui.textButtonStyle, 20,
				new ClickListener() {
					@Override
					public void clicked(InputEvent event, float x, float y) {
						menu.toggle();
						buildingMenu.hide();
						selectedBarracks = null;
					}
				});

		for (String barrackType : game.constants.getBarrackTypes()) {
			menu.addButton(new BuildButton(barrackType, ui.textButtonStyle,
					game, this));
		}

		Button upgrade = ui.createButton("Upgrade", ui.textButtonStyle, 10, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
								if (!game.state.upgradableBarracks(
										game.state.localPlayer,
										selectedBarracks.minionType)) {
									// don't upgrade when you can't
									// TODO dat userovi nejako vediet ze to co chce spravit sa neda
									return;
								}
								BarracksUpgradePacket paket =
										new BarracksUpgradePacket(
												selectedBarracks, game);
								if (server.isServer) {
									for (int i = 0; i < server.connections
											.size(); i++) {
										server.connections.get(i)
												.sendTCP(paket);
									}
								} else {
									client.client.sendTCP(paket);
								}
								game.state.barracksToUpgrade.add(paket);
			}
		});

		Button changeBuildingRoute = ui.createButton("Change route", ui.textButtonStyle, 10, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				ChangeRoutePacket paket = new ChangeRoutePacket(selectedBarracks, game);
				if (server.isServer) {
					for (int i = 0; i < server.connections.size(); i++) {
						server.connections.get(i).sendTCP(paket);
					}
				} else {
					client.client.sendTCP(paket);
				}
				game.state.routesToChange.add(paket);
			}
		});

		buildingMenu.addButton(changeBuildingRoute);
		buildingMenu.addButton(upgrade);

		buildMenuButton.setBounds(10, 10, buildMenuButton.getWidth() + 20,
				buildMenuButton.getHeight() + 20);



		stage.addActor(buildMenuButton);
		stage.addActor(menu);
		stage.addActor(buildingMenu);
	}

	@Override
	public void resize(int width, int height) {
		stage.setViewport(800, 480, false);
		stage.setCamera(game.camera);
	}

	void handleNetwork() {
		if (server.isServer) {
			// postavila sa budova
			while (server.toBuild.size() > 0) {
				NewBuildingPacket b = server.toBuild.pop();
				if (b.primaryType.equals("barracks")) {
					game.state.createBarracks(
							game.state.players.get(b.playerId),
							b.secondaryType, b.x, b.y, b.step);
				} else if (b.primaryType.equals("tower")) {
					// TODO: rework vezi?
					new TestTower(game.state.players.get(b.playerId), b.x, b.y, 	game, b.step);
				}
			}
			// zmenila sa trasa
			while (server.routesToChange.size() > 0) {
				game.state.routesToChange.add(server.routesToChange.pop());
			}
			while (server.barracksToUpgrade.size() > 0) {
				game.state.barracksToUpgrade
						.add(server.barracksToUpgrade.pop());
			}
		} else {
			// postavila sa budova
			while (client.toBuild.size() > 0) {
				NewBuildingPacket b = client.toBuild.pop();
				if (b.primaryType.equals("barracks")) {
					game.state.createBarracks(
							game.state.players.get(b.playerId),
							b.secondaryType, b.x, b.y, b.step);
				} else if (b.primaryType.equals("tower")) {
					// TODO: rework vezi?
					new TestTower(game.state.players.get(b.playerId), b.x, b.y,
							game, b.step);
				}
			}
			// zmenila sa trasa
			while (client.routesToChange.size() > 0) {
				game.state.routesToChange.add(client.routesToChange.pop());
			}
			while (client.barracksToUpgrade.size() > 0) {
				game.state.barracksToUpgrade
						.add(client.barracksToUpgrade.pop());
			}
		}
	}

	void drawBuildingUnderCursor(SpriteBatch batch) {
		if (building) {
			Vector3 touchPos = new Vector3();
			int gx = Gdx.input.getX();
			int gy = Gdx.input.getY();
			touchPos.set(gx, gy, 0);
			game.camera.unproject(touchPos);
			int x = (int) touchPos.x;
			int y = (int) touchPos.y;
			int ts = game.state.getTileSize();
			x = x - x % ts;
			y = (y - y % ts);
			batch.draw(toBeBuilt, x, y);
		}
	}

	@Override
	public void render(float delta) {
		game.camera.update();
		batch.setProjectionMatrix(game.camera.combined);
		// handle network-based events (send our information and process
		// received information)
		handleNetwork();
		// Adds delta to GameState internal timer
		if (game.state.step(delta)) {
			if (game.state.gameOver()) {
				game.setScreen(new GameOverScreen(game, game.state
						.winningPlayer()));
				if (server.isServer) {
					server.closeServer();
				} else {
					client.closeClient();
				}
			} else {
				if (server.isServer) {
					if (server.server.getConnections().length == 0) {
						game.setScreen(new GameOverScreen(game, null));
						server.closeServer();
					}
				} else {
					if (!client.client.isConnected()) {
						game.setScreen(new GameOverScreen(game, null));
						client.closeClient();
					}
				}
			}
		}

		// TODO spravit handlovanie pohybu kamery

		batch.begin();
		// Vykreslit spodok
		game.state.drawAll(vp, batch);
		// Vykreslit stavajucu sa budovu
		drawBuildingUnderCursor(batch);
		// Vykreslenie Goldov
		ui.getDefaultFont().draw(batch, "Gold: " + game.state.localPlayer.goldCount, 10, 165);

		StringBuilder kills = new StringBuilder();
		StringBuilder castleHPs = new StringBuilder();
		for (Player p : game.state.players) {
			kills.append(p.killCount);
			castleHPs.append(p.castle.hp);
			if (p.id != game.state.players.size()-1) {
				kills.append("/");
				castleHPs.append("/");
			}
		}
		// Vykreslenie počet zabitých minionov
		ui.getDefaultFont().draw(batch, "Kills: " + kills, 10, 180);
		// Vykreslenie počtu HP
		ui.getDefaultFont().draw(batch, "Castle hp: " + castleHPs, 10, 195);
		// vykreslenie routy
		if (selectedBarracks != null) {
			ui.getDefaultFont().draw(batch,
					"Route: " + selectedBarracks.getRouteNumber(), 650, 180);
		}
		batch.end();
		// Nad to vykreslit UI-cko
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void show() {
		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(stage);
		multiplexer.addProcessor(buildingClickProcessor);
		// TODO: Add click-on-buildings + move-map input processors
		Gdx.input.setInputProcessor(multiplexer);
	}

	/**
	 * sends info over TCP to other players that they should build building
	 *
	 * @param x
	 *            - x coordinate for building
	 * @param y
	 *            - y coordinate for building
	 * @param b
	 *            - building
	 */
	void informOthers(NewBuildingPacket paket) {
		if (server.isServer) {
			paket.playerId = 0;
			for (int i = 0; i < server.connections.size(); i++) {
				server.connections.get(i).sendTCP(paket);
			}
		} else {
			paket.playerId = game.state.localPlayer.id;
			client.client.sendTCP(paket);
		}
	}

	void informOthers(int x, int y, Barracks b) {
		NewBuildingPacket paket = new NewBuildingPacket();
		paket.x = x;
		paket.y = y;
		paket.primaryType = "barracks";
		paket.secondaryType = b.minionType;
		paket.step = b.buildStep;
		informOthers(paket);
	}

	void informOthers(int x, int y, Tower t) {
		NewBuildingPacket paket = new NewBuildingPacket();
		paket.x = x;
		paket.y = y;
		paket.primaryType = "tower";
		paket.secondaryType = "test";
		paket.step = t.buildStep;
		informOthers(paket);
	}

	class MyInputProcessor implements InputProcessor {

		@Override
		public boolean keyDown(int keycode) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean keyUp(int keycode) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean keyTyped(char character) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer,
				int button) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			//TODO prerobiť čísla na nejakú konštantu branú odniekadiaľ
			Vector3 vx = new Vector3(screenX, screenY, 1);
			game.camera.unproject(vx);
			int tx = (int) vx.x / game.state.getTileSize();
			int ty = (int) vx.y / game.state.getTileSize();
			if (tx < 0 || tx >= 25 || ty < 0 || ty >= 15) {
				return false;
			}
			Barracks b = game.state.grid.getBuildingFromCoords(tx, ty);
			if (b != null) {
				menu.hide();
				buildingMenu.show();
				selectedBarracks = b;
			}else {
				buildingMenu.hide();
				selectedBarracks = null;
			}
			return false;
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean mouseMoved(int screenX, int screenY) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean scrolled(int amount) {
			// TODO Auto-generated method stub
			return false;
		}

	}

}
