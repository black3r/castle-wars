package com.matfyz.castlewars.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.matfyz.castlewars.CastleWarsGame;
import com.matfyz.castlewars.Player;

public class GameOverScreen extends AbstractScreen {

	private Label heading;
	private Label winnerIs;
	private TextButton backToMainMenu;
	private Table table;

	public GameOverScreen(CastleWarsGame game, Player winner) {
		super(game);

		backToMainMenu = ui.createBackButton();
		heading = ui.createTextLabel("Game Over!", 3);
		if(winner != null) {
			winnerIs = ui.createTextLabel("Winner is: " + winner.name, 1);
		}
		else {
			winnerIs = ui.createTextLabel("Enemy player disconected", 1);
		}
		table = createTable();
		stage.addActor(table);
	}

	//Adds all components to visible menu and returns it
	private Table createTable() {
		Table table = new Table();
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		table.add(heading);
		table.getCell(heading).spaceBottom(50);
		table.row();
		Table t = new Table();
		t.size(300, 50);
		t.add(winnerIs).fill();
		t.row();
		t.add(backToMainMenu).fill();
		table.add(t);
		table.debug();
		return table;
	}

}
