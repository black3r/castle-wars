package com.matfyz.castlewars.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.matfyz.castlewars.CastleWarsGame;

/**
 * Screen should looks good. During his acting period fonts and skins should be
 * loaded.
 */
public class SplashScreen extends AbstractScreen {
	private Texture splashTexture;
	private Image splashImage;
	boolean loaded;

	public SplashScreen(CastleWarsGame game) {
		super(game);
	}

	@Override
	public void show() {
		super.show();

		loadAssets();

		// load the texture with the splash image
		splashTexture = new Texture("splashScreenCastle.png");

		// set the linear texture filter to improve the image stretching
		splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		// in the image atlas, our splash image begins at (0,0) of the
		// upper-left corner and has a dimension of 512x301
		TextureRegion splashRegion = new TextureRegion(splashTexture, 0, 0,
				512, 301);

		// here we create the splash image actor; its size is set when the
		// resize() method gets called
		splashImage = new Image(splashRegion);

		// this is needed for the fade-in effect to work correctly; we're just
		// making the image completely transparent
		splashImage.setColor(splashImage.getColor().r,
				splashImage.getColor().g, splashImage.getColor().b, 0f);

		// configure the fade-in/out effect on the splash image
		Action a = new Action() {

			@Override
			public boolean act(float delta) {
				loaded = true;
				return loaded;
			}
		};

		SequenceAction actions = new SequenceAction(Actions.fadeIn(0.45f),
				Actions.delay(0.5f), Actions.fadeOut(0.45f),a);
		splashImage.addAction(actions);

		// and finally we add the actor to the stage
		stage.addActor(splashImage);

	}

	@Override
	public void render(float delta) {
		super.render(delta);
		if (game.assetManager.update() && loaded) {
			ui.createUi();
			game.setScreen(new MainMenuScreen(game));
		}
	}

	private void loadAssets() {

		game.assetManager.load("ui/button.pack", TextureAtlas.class);
		game.assetManager.load("ui/uiskin.atlas", TextureAtlas.class);
		game.assetManager.load("ui/default.fnt", BitmapFont.class);
		game.assetManager.load("background.png", Texture.class);
		game.assetManager.load("sprites/budova.png", Texture.class);
		game.assetManager.load("sprites/minion.png", Texture.class);
		game.assetManager.load("sprites/tower.png", Texture.class);
		game.assetManager.load("sprites/bullet.png", Texture.class);
		game.assetManager.load("sprites/buildings/castle.png", Texture.class);

		loadHumanUnits();
		loadHumanBuildings();
		loadOrcBuildings();
		loadOrcUnits();
	}

	private void loadHumanBuildings() {
		String pathToHumanBuildings = "sprites/buildings/human/";

		game.assetManager.load(pathToHumanBuildings + "meleebarracks.png",
				Texture.class);
		game.assetManager.load(pathToHumanBuildings + "artillerybarracks.png", Texture.class);
		game.assetManager.load(pathToHumanBuildings + "flyingbarracks.png", Texture.class);
		game.assetManager.load(pathToHumanBuildings + "magicbarracks.png", Texture.class);
		game.assetManager.load(pathToHumanBuildings + "rangebarracks.png", Texture.class);
		game.assetManager.load(pathToHumanBuildings + "mountedbarracks.png", Texture.class);
	}

	private void loadHumanUnits() {
		String pathToHumanUnits = "sprites/units/human/";

		game.assetManager.load(pathToHumanUnits + "admiral.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "apprentice.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "archmage.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "artillery.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "catapult.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "commander.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "crossbowman.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "fighter.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "flyingmachine.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "general.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "horseman.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "knight.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "mage.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "peasant.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "rifleman.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "scorpion.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "scout.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "slingshotter.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "warrior.png", Texture.class);
		game.assetManager.load(pathToHumanUnits + "zeppelin.png", Texture.class);
	}

	private void loadOrcBuildings() {
		String pathToorcBuildings = "sprites/buildings/orc/";

		game.assetManager.load(pathToorcBuildings + "meleebarracks.png", Texture.class);
		game.assetManager.load(pathToorcBuildings + "artillerybarracks.png", Texture.class);
		game.assetManager.load(pathToorcBuildings + "flyingbarracks.png", Texture.class);
		game.assetManager.load(pathToorcBuildings + "magicbarracks.png", Texture.class);
		game.assetManager.load(pathToorcBuildings + "rangebarracks.png", Texture.class);
		game.assetManager.load(pathToorcBuildings + "mountedbarracks.png", Texture.class);
	}

	private void loadOrcUnits() {
		String pathToOrcUnits = "sprites/units/orc/";

		game.assetManager.load(pathToOrcUnits + "grunt.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "barserker.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "axethrower.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "spearthrower.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "wolfrider.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "wolfcavalry.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "batteringram.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "armoredram.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "wyvern.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "windrider.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "shaman.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "spiritwalker.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "warchief.png", Texture.class);
		game.assetManager.load(pathToOrcUnits + "blademaster.png", Texture.class);
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		// resize the splash image
		splashImage.setWidth(width);
		splashImage.setHeight(height);

		// we need a complete redraw
		splashImage.invalidateHierarchy();
	}

	@Override
	public void dispose() {
		super.dispose();
		splashTexture.dispose();
	}
}
