package com.matfyz.castlewars.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.matfyz.castlewars.CastleWarsGame;
import com.matfyz.castlewars.UI;

/**
 * The base class for all game screens.
 */
public abstract class AbstractScreen implements Screen {
	protected static UI ui;
	protected final CastleWarsGame game;
	protected final Stage stage;

	private BitmapFont font;
	private SpriteBatch batch;
	public int width, height;

	public AbstractScreen(CastleWarsGame game) {
		this.game = game;
		this.stage = new Stage(0, 0, true);
		if (AbstractScreen.ui == null) {
			AbstractScreen.ui = new UI(game);
		}
	}

	protected String getName() {
		return getClass().getSimpleName();
	}

	public BitmapFont getFont() {
		if (font == null) {
			font = new BitmapFont();
		}
		return font;
	}

	public SpriteBatch getBatch() {
		if (batch == null) {
			batch = new SpriteBatch();
		}
		return batch;
	}


	// Screen implementation

	@Override
	public void show() {
		// set the stage as the input processor
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void resize(int width, int height) {
		// resize the stage
		stage.setViewport(width, height, false);
		this.width = width;
		this.height = height;
	}

	@Override
	public void render(float delta) {
		// update the actors
		//game.camera.update();
		//getBatch().setProjectionMatrix(game.camera.combined);
		stage.act(delta);

		// clear the screen with the given RGB color (black)
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// draw the actors
		stage.draw();
	}

	@Override
	public void hide() {
		// dispose the resources by default
		dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
		if (font != null) {
			font.dispose();
		}
		if (batch != null) {
			batch.dispose();
		}
	}
}
