package com.matfyz.castlewars.screens;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.matfyz.castlewars.CastleWarsGame;
import com.matfyz.castlewars.Player;
import com.matfyz.castlewars.UI.ConnectToHostDialog;
import com.matfyz.castlewars.networking.ConnectionRequestPacket;
import com.matfyz.castlewars.networking.MyClient;



public class JoinGameScreen extends AbstractScreen {

	TextButton connect, backToMainMenu;
	TextField textField;
	private Table table;
	private Label heading;
	private MyClient client;

	public JoinGameScreen(final CastleWarsGame game) {
		super(game);
		textField = ui.createTextfield("Name", 20, 60);
		connect = ui.createButton("Connect", ui.textButtonStyle, 20, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x,
					float y) {
				connectToServer();
			}
		});
		backToMainMenu = ui.createBackButton();
		heading = ui.createTextLabel("Join Game", 3);
		table = createTable();
		stage.addActor(table);
	}

	//Send connection request to server (specified by tcpPort and udpPort)
	public void connectToServer() {
		client = MyClient.getInstance();
		ConnectionRequestPacket connectionRequest = new ConnectionRequestPacket();
		connectionRequest.player = new Player();
		connectionRequest.player.name = textField.getText();
		connectionRequest.player.race = "Human";
		List<InetAddress> hosts = client.getPossibleHosts();
		List<InetAddress> newHosts = new ArrayList<InetAddress>();
		for (InetAddress addr : hosts) {
			if (!newHosts.contains(addr)) {
				newHosts.add(addr);
			}
		}
		ConnectToHostDialog d = ui.createSelectHostDialog("Select host", newHosts, connectionRequest);
		d.show(stage);
	}

	//Adds all components to visible menu and returns it
	private Table createTable() {
		Table table = new Table();
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		table.add(heading);
		table.getCell(heading).spaceBottom(50);
		table.row();
		Table t = new Table();
		t.size(300, 50);
		t.add(textField).fill();
		t.row();
		t.add(connect).fill();
		t.row();
		t.add(backToMainMenu).fill();
		table.add(t);
		table.debug();
		return table;
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		if (client != null && client.connected) {
			game.setScreen(new LobbyScreen(game, null, "client", null));
		}
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		table.setSize(width, height);
		table.invalidateHierarchy();
	}

}
