package com.matfyz.castlewars.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.matfyz.castlewars.CastleWarsGame;

/**
 * MainMenu represents navigation between basic screens like HostGameScreen,
 * JoinGameScreen , Settings.
 *
 * Displays right after SplashScreen.
 */
public class MainMenuScreen extends AbstractScreen {
	private Table table;
	private TextButton buttonExit, buttonHostGame, buttonJoinGame,
			buttonSettings, buttonDemoGame;
	private Label heading, label;

	public MainMenuScreen(CastleWarsGame game) {
		super(game);
		buttonHostGame =
				ui.createScreenButton("Host Game", ui.textButtonStyle, 10,
				new HostGameScreen(game));
		buttonJoinGame =
				ui.createScreenButton("Join Game", ui.textButtonStyle, 10,
				new JoinGameScreen(game));
		buttonSettings =
				ui.createScreenButton("Settings", ui.textButtonStyle, 10,
				new SettingsScreen(game));
		buttonExit =
				ui.createScreenButton("Exit", ui.textButtonStyle, 10, null);
		label =
				ui.createTextLabel("Version: " + game.constants.getVersion(),
 1);
		label.setX(10);
		label.setY(height - 30);

		// TODO pohrať sa s písmom (fontami) aby to nebolo astrované
		heading = ui.createTextLabel("Castle Wars", 3);
		table = createTable();
		stage.addActor(table);
		stage.addActor(label);
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		table.setSize(width, height);
		table.invalidateHierarchy();
		label.setY(height - 30);
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	private Table createTable() {
		Table table = new Table();
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		table.add(heading);
		table.getCell(heading).spaceBottom(50);
		table.row();
		Table t = new Table();
		t.size(300, 50);
		t.add(buttonDemoGame).fill();
		t.row();
		t.add(buttonHostGame).fill();
		t.row();
		t.add(buttonJoinGame).fill();
		t.row();
		t.add(buttonSettings).fill();
		t.row();
		t.add(buttonExit).fill();
		table.add(t);
		table.debug();
		return table;
	}
}
