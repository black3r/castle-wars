package com.matfyz.castlewars.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.matfyz.castlewars.CastleWarsGame;
import com.matfyz.castlewars.Player;
import com.matfyz.castlewars.networking.MyClient;
import com.matfyz.castlewars.networking.MyServer;
import com.matfyz.castlewars.networking.StartGamePacket;

public class LobbyScreen extends AbstractScreen {
	MyServer server;
	MyClient client;
	String hostName;
	int tcpPort, udpPort;
	String gameType, connectionType;
	ArrayList<Player> connectedPlayers;
	Table container;
	Label info;
	Button startGame;
	List uiPlayerList;
	int lastNumOfPlayers = 0;
	boolean raceChanged = false;

	// can be called with gameType == null only when connectionType == "client"
	public LobbyScreen(final CastleWarsGame game, String gameType,
			String connectionType, String hostName) {
		super(game);
		this.hostName = hostName;
		this.gameType = gameType;
		this.connectionType = connectionType;
		connectedPlayers = new ArrayList<Player>();
		container = new Table();

		if (connectionType.equals("server")) {
			// Creating server and getting free ports
			createServer();
			info = ui.createTextLabel("Game has been created!", 1);
			startGame = ui.createButton("Start Game", ui.textButtonStyle, 20, new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					game.setScreen(new InGameScreen(game));
					for (int i = 0; i < server.connections.size(); i++) {
						server.connections.get(i).sendTCP(new StartGamePacket());
					}
				}
			});
		} else {
			createClient();
			info = ui.createTextLabel("Successfully joined game, welcome", 1);
		}

		// Creating UI

		container = createContainer();
		stage.addActor(container);
	}

	/**
	 * Adds ui widgets to one table component
	 *
	 * @return ui
	 */
	private Table createContainer() {
		Table container = new Table();
		container.setBounds(0, 0, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		container.row();
		Table t = new Table();
		t.size(300, 50);
		t.row();
		t.add(info).fill();
		t.row();
		t.add(uiPlayerList).fill();
		container.add(t);
		t.row();
		t.add(startGame);
		t.row();
		t.add(ui.createButton("<- Back", ui.textButtonStyle, 10,
				new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (connectionType.equals("server")) {
					server.closeServer();
				} else {
					client.closeClient();
				}
				game.setScreen(new MainMenuScreen(game));
			}
		}));
		return container;
	}

	public void createServer() {
		server = MyServer.getInstance(gameType);
		server.isServer = true;
		Player p = new Player(hostName, "Human");
		p.id = 0;
		server.players.add(p);
	}

	public void createClient() {
		client = MyClient.getInstance();
	}

	@Override
	public void render(float delta) {
		checkRaceChange();
		checkNewPlayers();
		refindConnectedPlayers();
		if (lastNumOfPlayers < connectedPlayers.size() || raceChanged) {
			lastNumOfPlayers = connectedPlayers.size();
			raceChanged = false;
			container = createContainer();
			stage.clear();
			stage.addActor(container);
		}
		if (connectionType.equals("client") && client.gameStarted) {
			game.setScreen(new InGameScreen(game));
		}
		super.render(delta);
	}

	private void checkRaceChange() {
		if (connectionType.equals("server")) {
			if (server.raceChanged) {
				connectedPlayers = server.players;
				server.raceChanged = false;
				raceChanged = true;
			}
		} else {
			if (client.raceChanged) {
				connectedPlayers = client.players;
				client.raceChanged = false;
				raceChanged = true;
			}
		}
	}

	private void checkNewPlayers() {
		if (connectionType.equals("server")) {
			int diff = server.players.size() - connectedPlayers.size();
			if (diff > 0) {
				for (int i = connectedPlayers.size(); i < server.players.size(); i++) {
					Player newPlayer = server.players.get(i);
					connectedPlayers.add(newPlayer);
				}
			}
		} else {
			int diff = client.players.size() - connectedPlayers.size();
			if (diff > 0) {
				for (int i = connectedPlayers.size(); i < client.players.size(); i++) {
					Player newPlayer = client.players.get(i);
					connectedPlayers.add(newPlayer);
				}
			}
		}
	}

	/**
	 * Refresh (ui)List with connectedPlayers
	 */
	private void refindConnectedPlayers() {
		Array<String> strPlayers = new Array<String>();
		for (int i = 0; i < connectedPlayers.size(); i++) {
			strPlayers.add(connectedPlayers.get(i).toString());
		}
		uiPlayerList = ui.createList(strPlayers.toArray(), stage);
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		container.setSize(width, height);
		container.invalidateHierarchy();
	}
}
