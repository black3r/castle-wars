package com.matfyz.castlewars;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Class containing global gameplay constants (and variables), like minion
 * stats, game speed, etc.. Constants themselves are locally stored in a Map of
 * Strings. Every used constant should have a getter function, especially if it
 * is supposed to work with other data types on higher level.
 */
public class GameConstants {
	/**
	 * Key-Value map containing constants as Strings
	 */
	private ObjectMap values;
	private Json json;

	public Integer getInteger(Object key) {
		return (int) (float) (Float) values.get(key.toString());
	}

	public int getInt(Object key) {
		return getInteger(key).intValue();
	}

	public String getString(Object key) {
		return (String) values.get(key.toString());
	}

	/**
	 * Converts JsonValue to Array of Integers. Serves as an example of
	 * converting to other array types.
	 *
	 * @param val
	 * @return
	 */
	public Array<Integer> toArrayOfInteger(JsonValue val) {
		return json.fromJson(Array.class, Integer.class, val.toString());
	}

	public Array<Integer> getArrayOfInteger(Object key) {
		return toArrayOfInteger((JsonValue) values.get(key));
	}

	public Array<String> toArrayOfString(JsonValue val) {
		return json.fromJson(Array.class, String.class, val.toString());
	}

	public Array<String> getArrayOfString(Object key) {
		return toArrayOfString((JsonValue) values.get(key));
	}

	/**
	 * Default constructor loads the values from file constants.cfg
	 */
	public GameConstants() {
		FileHandle file = Gdx.files.internal("constants.cfg");
		json = new Json();
		values = json.fromJson(ObjectMap.class, file);
	}

	/**
	 * Obtains the game version as string
	 *
	 * @return String game version
	 */
	public String getVersion() {
		return getString("version");
	}

	/**
	 * Obtains the number of routes as Integer
	 *
	 * @return Integer number of routes
	 */
	public Integer getNumberOfRoutes() {
		return getInteger("numberOfRoutes");
	}

	/**
	 * Returns the number of steps which building must wait before
	 * spawning a minion again.
	 * @return
	 */
	public Integer getBuildingCooldown() {
		return getInteger("buildingCooldown");
	}

	/**
	 * Returns game step duration (in milliseconds).
	 * @return
	 */
	public Integer getStepDuration() {
		return getInteger("stepDuration");
	}

	/**
	 * Obtains the X or Y axis of route for minion as Array
	 *
	 * @param routeNumber
	 *            - route number
	 * @param axis
	 *            - x or y axis
	 * @return array of integers of Xs or Ys of specified route
	 */
	@SuppressWarnings("unchecked")
	public Array<Integer> getRoute(int routeNumber, String axis) {
		// Example of nested getting from values. You convert the initially
		// obtained object, and cast it to JsonValue. Then you can get
		// and get, afterwards use cast if getting basic type, or
		// json.fromJson() if getting nested types (Array / ObjectMap)
		return toArrayOfInteger(((JsonValue) values.get("route")).get(
				String.valueOf(routeNumber)).get(axis));
	}

	public JsonValue getUnits(String race) {
		return  ((JsonValue)values.get("units")).get(race);
	}

	public JsonValue getBarracks(String race) {
		return ((JsonValue) values.get("barracks")).get(race);
	}

	public JsonValue getCastle(String race) {
		return ((JsonValue) values.get("castle")).get(race);
	}

	public int getStartingGold() {
		return getInt("startingGold");
	}

	public Array<String> getBarrackTypes() {
		return (Array<String>) values.get("barrackTypes");
		//return getArrayOfString("barrackTypes");
	}
}
