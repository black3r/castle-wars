package com.matfyz.castlewars;

/**
 * Contains information about current game view (position in map and view size,
 * in pixels).
 */
public class Viewport {
	public int x, y, width, height;

	public Viewport(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

}
