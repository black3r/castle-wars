package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;

public class TestTower extends Tower {

	public TestTower(Player owner, int x, int y, CastleWarsGame game) {
		super(owner, x, y, 32, (Texture) game.assetManager
				.get("sprites/tower.png"), 100, 10, game);

		// TODO: sane weapon ranges
		this.weaponRange = 50;
		this.dmg = 25;
	}

	public TestTower(Player owner, int x, int y, CastleWarsGame game,
			long buildStep) {
		super(owner, x, y, 32, (Texture) game.assetManager
				.get("sprites/tower.png"), 100, 10, game, buildStep);

		// TODO: sane weapon ranges
		this.weaponRange = 50;
		this.dmg = 25;
	}

}
