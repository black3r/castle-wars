package com.matfyz.castlewars;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Class contains game configuration settings, which are meant
 * to be saved when the game exits and loaded when the game
 * is started. Save/Load is implemented by LibGDX internal
 * Preferences mechanism.
 *
 * All configuration variables should have their own methods here,
 * so the front-end code is nice and we can use auto-complete when
 * designing it without worrying :)
 */
public class GameConfig {
	/**
	 * Global preferences for this game
	 */
	Preferences prefs = Gdx.app.getPreferences("CastleWars");

	/**
	 * Saves the set preferences immediately
	 */
	public void save() {
		prefs.flush();
	}

	/**
	 * Obtains value for selected key
	 * @param key String
	 * @return String
	 */
	public String get(String key) {
		return prefs.getString(key);
	}

	/**
	 * Sets the value for selected key. Remember to .save() afterwards!
	 * @param key String
	 * @param value String
	 */
	public void put(String key, String value) {
		prefs.putString(key,  value);
	}
}
