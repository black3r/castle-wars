package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;

/**
 * Class contains methods for building an building
 *
 * Extends :
 * Destroyable(hyperlink)
 *
 * Extended by:
 * Tower(hyperlink)
 * Castle(hyperlink)
 * Barracks(hyperlink)
 * concrete buildings later..
 */
public abstract class Building extends Destroyable {
	public Building(Player owner, int x, int y, int tileSize, Texture sprite,
			int hp,
 int dmg, CastleWarsGame game) {
		super(owner, x, y, tileSize, sprite, hp, dmg, game);
		this.buildStep = game.state.stepCount + 2;
		game.state.registerBuilding(this);
	}

	public Building(Player owner, int x, int y, int tileSize, Texture sprite,
			int hp, int dmg, CastleWarsGame game, long buildStep) {
		super(owner, x, y, tileSize, sprite, hp, dmg, game);
		this.buildStep = buildStep;
		game.state.registerBuilding(this);
	}

	/**
	 * Performs one game step. (i.e. Towers do the findTarget, attackTarget
	 * stuff, Barracks build Minions if ready, etc..)
	 */
	abstract void act();

	public long buildStep;

	public boolean equal(Building b) {
		return this.x == b.x && this.y == b.y;
	}
}
