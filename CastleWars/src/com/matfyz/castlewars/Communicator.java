package com.matfyz.castlewars;

import java.util.Observable;

import com.badlogic.gdx.utils.Array;

/**
 * Class maintaining networking and communication.
 * Contatins list of all players(RemotePlayer class)
 * and notify them about changes.
 * 
 * Extends:
 * Observable(hyperlink)
 * 
 * Implements:
 * RemoteCommands(hyperlink)
 */
public class Communicator extends Observable implements RemoteCommands{
	
	Array<RemotePlayer> players = new Array<RemotePlayer>();
	
	public Communicator() {
		
	}
	
	@Override
	public void broadcastChange() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void broadcastPause() {
		// TODO Auto-generated method stub
		
	}

}
