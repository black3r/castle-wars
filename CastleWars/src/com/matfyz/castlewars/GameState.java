package com.matfyz.castlewars;

import java.util.ArrayList;
import java.util.LinkedList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonValue;
import com.matfyz.castlewars.networking.BarracksUpgradePacket;
import com.matfyz.castlewars.networking.ChangeRoutePacket;

/**
 * Contains information about game currently in-progress. Is centralized
 * interface for information about living minions, standing buildings, upgraded
 * upgrades, etc... Actual storage is stored in different classes.
 *
 * Can be serialized into JSON for save/load.
 */
public class GameState {
	public Destroyable findNearestEnemy(int x, int y, Player currentPlayer) {
		return grid.findNearestEnemy(x, y, currentPlayer);
	}

	public Destroyable findNearestEnemyCloserThan(int x, int y,
			Player currentPlayer, int maxDistance) {
		return grid
				.findNearestEnemyCloserThan(x, y, currentPlayer, maxDistance);
	}

	public int distanceFrom(int xx, int yy, Destroyable targett) {
		return Math.abs(targett.x - xx) + Math.abs(targett.y - yy);
	}

	/**
	 * Represents route through map which is saved in gameConstants
	 */
	class Route {
		int routeNumber;
		Array<Integer> routeX;
		Array<Integer> routeY;

		public Route(int routeNumber) {
			this.routeNumber = routeNumber;
			routeX = game.constants.getRoute(routeNumber,"X");
			routeY = game.constants.getRoute(routeNumber,"Y");
		}
	}

	protected final CastleWarsGame game;
	public GameGrid grid;
	public ArrayList<Player> players;
	public Player localPlayer;
	// TODO: enemyPlayer je debugovacia premenna
	public Player enemyPlayer;
	public Array<Building> buildings = new Array<Building>();
	public LinkedList<Minion> minions = new LinkedList<Minion>();
	public LinkedList<Projectile> projectiles = new LinkedList<Projectile>();
	public int numberOfRoutes;
	public Array<Route> routes = new Array<GameState.Route>();
	public float time = 0;
	public long stepCount = 0;

	public GameState(CastleWarsGame game, int gridWidth, int gridHeight,
			int gridTileSize, Texture mapBackground, ArrayList<Player> players,
			Player localPlayer) {
		this.game = game;
		this.grid =
				new GameGrid(gridWidth, gridHeight, gridTileSize, mapBackground);
		this.players = players;
		this.localPlayer = localPlayer;
		numberOfRoutes = game.constants.getNumberOfRoutes();
		// TODO: Refactor: tu, aj v constants.cfg precislovat routes od 0
		for (int i = 0; i < numberOfRoutes; i++) {
			routes.add(new Route(i));
		}
	}

	public void createPlayers() {
		for (Player p : players) {
			createPlayerCastle(p, game);
			p.goldCount = game.constants.getStartingGold();
			for (String type : game.constants.getBarrackTypes()) {
				p.buildingLevels.put(type, 0);
			}
		}
	}

	private void createPlayerCastle(Player owner, CastleWarsGame game) {
		JsonValue castleInfo = game.constants.getCastle(owner.race);
		int y = 8;
		int x = 0;
		if (owner.id == 1) {
			x = 23;
		}
		int tileSize = castleInfo.getInt("tileSize");
		Texture sprite =
				game.assetManager.get(castleInfo.getString("sprite"),
						Texture.class);
		int hp = castleInfo.getInt("hp");
		int dmg = castleInfo.getInt("dmg");
		owner.castle = new Castle(owner, x, y, tileSize, sprite, hp, dmg, game);
	}

	public LinkedList<Projectile> removedProjectiles =
			new LinkedList<Projectile>();
	public LinkedList<Minion> removedMinions = new LinkedList<Minion>();
	public LinkedList<Building> removedBuildings = new LinkedList<Building>();

	public LinkedList<Building> buildingsToBuild = new LinkedList<Building>();

	public void flagRemoved(Minion m) {
		removedMinions.add(m);
	}

	public void flagRemoved(Building b) {
		removedBuildings.add(b);
	}

	public void flagRemoved(Projectile p) {
		removedProjectiles.add(p);
	}

	public void reapMinions() {
		for (Minion minion : removedMinions) {
			removeMinion(minion);
		}
		removedMinions.clear();
	}

	public void reapBuildings() {
		for (Building building : removedBuildings) {
			removeBuilding(building);
		}
		removedBuildings.clear();
	}

	public void reapProjectiles() {
		for (Projectile projectile : removedProjectiles) {
			removeProjectile(projectile);
		}
		removedProjectiles.clear();
	}

	public int getTileSize() {
		return grid.tilesize;
	}

	/**
	 * Add delta to internal timer. If it's time for another GameStep, perform
	 * it. Internal timer should only remember what time passed since the
	 * previous actAll() call.
	 * @param delta
	 */
	public boolean step(float delta) {
		int oldTime = (int) (time * 1000);
		int newTime = (int) ((time + delta) * 1000);
		int stepDuration = game.constants.getStepDuration();
		int oldStep = oldTime / stepDuration;
		int newStep = newTime / stepDuration;
		boolean result = false;
		if (oldStep != newStep) {
			stepCount++;
			actAll();
			result = true;
		}
		newTime %= stepDuration;
		time = (float) newTime / 1000;
		return result;
	}

	/**
	 * Performs one GameStep on all minions.
	 */
	public void actMinions() {
		for (Minion minion : minions) {
			if (!minion.isDestroyed) {
				minion.act();
			}
		}
	}

	/**
	 * Performs one GameStep on all buildings.
	 */
	public void actBuildings() {
		for (Building building : buildings) {
			if (!building.isDestroyed) {
				building.act();
			}
		}
	}

	/**
	 * Performs one GameStep on all projectiles.
	 */
	public void actProjectiles() {
		for (Projectile projectile : projectiles) {
			projectile.act();
		}
	}

	/**
	 * returns [boolean] if game is over yet or not.
	 * @return
	 */
	public boolean gameOver() {
		for (Player p : players) {
			if (p.castle.isDestroyed) {
				return true;
			}
		}
		return false;
	}

	/**
	 * returns the player who won.
	 * This function now works only in Last Man Standing matches ;)
	 * @return
	 */
	public Player winningPlayer() {
		for (Player p : players) {
			if (!p.castle.isDestroyed) {
				return p;
			}
		}
		return null;
	}

	/**
	 * Performs one GameStep on all objects in-game.
	 */
	public void actAll() {
		//System.out.println("Ticking game");
		handleBuiltBuildings();
		handleUpgradedBuildings();
		handleChangeRoute();
		actBuildings();
		actMinions();
		actProjectiles();
		reapBuildings();
		reapMinions();
		reapProjectiles();
		// TODO add castle.act();
	}

	/**
	 * Registers the building with GameState and GameGrid.
	 * @param x
	 * @param y
	 * @param building
	 */
	public void addBuilding(int x, int y, Building building) {
		building.owner.buildings.add(building);
		this.buildings.add(building);
		grid.add(x, y, building);
	}

	public void registerBuilding(Building building) {
		this.buildingsToBuild.add(building);
	}

	public void handleBuiltBuildings() {
		LinkedList<Building> newlist = new LinkedList<Building>();
		for (Building b : buildingsToBuild) {
			if (b.buildStep == stepCount) {
				addBuilding(b.x, b.y, b);
			} else {
				newlist.add(b);
			}
		}
		buildingsToBuild = newlist;
	}

	public LinkedList<ChangeRoutePacket> routesToChange =
			new LinkedList<ChangeRoutePacket>();

	public LinkedList<BarracksUpgradePacket> barracksToUpgrade =
			new LinkedList<BarracksUpgradePacket>();

	public void handleUpgradedBuildings() {
		LinkedList<BarracksUpgradePacket> newlist =
				new LinkedList<BarracksUpgradePacket>();
		for (BarracksUpgradePacket p : barracksToUpgrade) {
			if (p.step == stepCount) {
				game.state.players.get(p.playerId).upgradeBuilding(
						p.barracksType);
			} else {
				newlist.add(p);
			}
		}
		barracksToUpgrade = newlist;
	}

	public void handleChangeRoute() {
		LinkedList<ChangeRoutePacket> newlist =
				new LinkedList<ChangeRoutePacket>();
		for (ChangeRoutePacket p : routesToChange) {
			if (p.step == stepCount) {
				for (Building b : game.state.players.get(p.playerId).buildings) {
					if (b instanceof Barracks) {
						if (((Barracks) b).minionType.equals(p.barracksType)) {
							((Barracks) b).changeRoute();
						}
					}
				}
			} else {
				newlist.add(p);
			}
		}
		routesToChange = newlist;
	}

	/**
	 * Corrects the minion spawn point if needed (minion cannot spawn
	 * on enemy minion or enemy building) and registers the minion with
	 * state and grid.
	 * @param x
	 * @param y
	 * @param minion
	 */
	public void spawnMinion(int x, int y, Minion minion) {
		this.minions.add(minion);
		grid.forceAdd(x, y, minion);
	}

	public void spawnProjectile(int x, int y, Projectile projectile) {
		this.projectiles.add(projectile);
	}

	/**
	 * Disassociates the selected unit from grid, removing references to it
	 * from grid.
	 * @param unit
	 */
	public void removeUnitFromGrid(Drawable unit) {
		this.grid.remove(unit);
	}

	/**
	 * Removes the minion from list of minions and from grid.
	 * @param minion
	 */
	public void removeMinion(Minion minion) {

		removeUnitFromGrid(minion);
		this.minions.remove(minion);
	}

	/**
	 * Removes the projectile from list of projectiles.
	 * @param projectile
	 */
	public void removeProjectile(Projectile projectile) {
		this.projectiles.remove(projectile);
	}

	/**
	 * Removes the building from list of buildings and from grid.
	 * @param building
	 */
	public void removeBuilding(Building building) {
		this.grid.remove(building);
		this.buildings.removeValue(building, true);
	}

	/**
	 * Mark the target as dead and remove the target from the game in the
	 * beginning of next game cycle.
	 * @param minion
	 */
	public void destroyMinion(Minion minion) {
		// TODO: Implement delayed reaping
		this.removeMinion(minion);
	}

	/**
	 * Mark the building as dead and remove the building from the game in the
	 * beginning of next game cycle.
	 * @param building
	 */
	public void destroyBuilding(Building building) {
		// TODO: implement delayed reaping
		this.removeBuilding(building);
	}

	/**
	 * Tries to move the specified drawable to specified location.
	 * If impossible, returns false.
	 * @param x
	 * @param y
	 * @param minion
	 * @return
	 */
	public boolean moveDrawable(int x, int y, Drawable unit) {
		boolean res = this.grid.moveTo(x, y, unit);
		return res;
	}

	private void drawBackground(Viewport vp, SpriteBatch batch) {
		grid.drawBackground(vp, batch);
	}

	private void drawProjectiles(Viewport vp, SpriteBatch batch) {
		for (Drawable d : projectiles) {
			d.draw(vp, batch);
		}
	}

	private void drawBuildings(Viewport vp, SpriteBatch batch) {
		for (Drawable d : buildings) {
			d.draw(vp, batch);
		}
	}

	private void drawMinions(Viewport vp, SpriteBatch batch) {
		for (Drawable d : minions) {
			d.draw(vp, batch);
		}
	}

	public void drawAll(Viewport vp, SpriteBatch batch) {
		drawBackground(vp, batch);
		drawMinions(vp, batch);
		drawBuildings(vp, batch);
		drawProjectiles(vp, batch);
	}

	public boolean canBuildBarracks(Player player, String barracksType, int x,
			int y) {
		if (!canHasCashForBarracks(player, barracksType)) {
			return false;
		}
		// TODO: budova sa moze stavat iba tam kde este nestoji ziadna budova
		if (grid.getBuildingFromCoords(x, y) != null) {
			return false;
		}
		// TODO: hrac moze stavat len na svojej "Tretine" herneho uzemia
		if (y < 1 || y > 13) {
			return false;
		}
		int tx = x;
		if (player.id != 0) {
			tx = 24 - x;
		}
		if (tx < 1 || tx > 8) {
			return false;
		}
		return true;
	}

	public boolean canHasCashForBarracks(Player player, String barracksType) {
		if (player.goldCount < game.constants.getBarracks(player.race)
				.get(barracksType).getInt("price")) {
			return false;
		}
		return true;
	}

	public boolean upgradableBarracks(Player player, String barracksType) {
		if (player.goldCount < 200) {
			// TODO: actually pouzit konstanty z constants.cfg
			return false;
		}
		if (player.getBuildingLevel(barracksType) + 1 == game.constants
				.getBarracks(player.race).get(barracksType).getInt("levels")) {
			return false;
		}
		return true;
	}

	public Barracks createBarracks(Player player, String barracksType, int x,
			int y) {
		player.goldCount -=
				game.constants.getBarracks(player.race).get(barracksType)
						.getInt("price");
		return new Barracks(player, x, y, game, barracksType);
	}

	public Barracks createBarracks(Player player, String barracksType, int x,
			int y, long stepCount) {
		player.goldCount -=
				game.constants.getBarracks(player.race).get(barracksType)
						.getInt("price");
		return new Barracks(player, x, y, game, barracksType, stepCount);
	}
}
