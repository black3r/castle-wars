package com.matfyz.castlewars;

import java.util.Observable;
import java.util.Observer;

/**
 * Class representing connection between players, contains all important
 * communication methods
 * 
 * Implements: Observer(hyperlink) RemoteCommands(hyperlink)
 */
public class RemotePlayer implements Observer, RemoteCommands {

	public RemotePlayer() {
		// TODO Auto-generated constructor stub
	}	
	
	@Override
	public void update(Observable arg0, Object arg1) {


	}

	@Override
	public void broadcastChange() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void broadcastPause() {
		// TODO Auto-generated method stub
		
	}

}
