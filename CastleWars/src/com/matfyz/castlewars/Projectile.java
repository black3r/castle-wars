package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;

/**
 * Class representing all prjectiles in game like flying stones/arrows etc..
 *
 * Implements: Drawable(hyperlink)
 *
 * Extended by: concrete projectiles
 */
public class Projectile extends Drawable {
	Destroyable target;
	Player owner; // the player who shot this projectile
	DamageType dmgType;
	int dmg;
	boolean done = false;

	public Projectile() {
		moveSpeed = 2; // TODO: dorobit moveSpeed;
	}

	public Projectile(Destroyable attacker, Destroyable target) {
		super(attacker.x, attacker.y, 32, (Texture) attacker.game.assetManager
				.get("sprites/bullet.png"), attacker.game);
		moveSpeed = 2; // TODO: dorobit moveSpeed;
		this.owner = attacker.owner;
		this.target = target;
		this.dmgType = attacker.dmgType;
		this.dmg = attacker.dmg;
		game.state.spawnProjectile(attacker.x, attacker.y, this);
	}

	public void act() {
		moving = false;
		if (!done) {
			if (target.isDestroyed) {
				game.state.flagRemoved(this);
				done = true;
			} else if (moveTowards(target)) {
				target.wasAttacked(this);
				game.state.flagRemoved(this);
				done = true;
			}
		}
	}
}
