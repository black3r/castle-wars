package com.matfyz.castlewars;

import java.util.LinkedList;

/**
 * Represents one tile in game grid. Can be occupied by a minion or a
 * building, or be empty.
 */
public class Tile {
	LinkedList<Drawable> inhabitants;
	int x, y;

	/**
	 * Checks if tile is empty (contains no inhabitants).
	 * @return
	 */
	boolean isEmpty() {
		return inhabitants.isEmpty();
	}

	/**
	 * If the tile is attackable, returns the first character that
	 * entered this tile. Else return null;
	 * @return
	 */
	Destroyable getTarget() {
		for (Drawable inhabitant : inhabitants) {
			if (inhabitant instanceof Destroyable) {
				return (Destroyable) inhabitant;
			}
		}
		return null;
	}

	/**
	 * Checks if inhabitant can be inserted and adds if possible.
	 * Returns the same as pollInhabitant();
	 * @param inhabitant
	 * @return
	 */
	boolean addInhabitant(Drawable inhabitant) {
		if (!pollInhabitant(inhabitant)) {
			return false;
		} else {
			inhabitants.add(inhabitant);
			inhabitant.x = this.x;
			inhabitant.y = this.y;
			return true;
		}

	}

	/**
	 * Removes (disassociates) the selected inhabitant from this tile.
	 * @param inhabitant
	 */
	void removeInhabitant(Drawable inhabitant) {
		if (!inhabitants.contains(inhabitant)) {
			if (inhabitant instanceof Minion) {
			} else if (inhabitant instanceof Projectile) {
			}

		}
		inhabitants.remove(inhabitant);
	}

	/**
	 * Checks if inhabitant can be inserted to this Tile.
	 * Inhabitant can be inserted if:
	 *  - Tile is empty
	 *  - Tile contains only drawable blocks (walls/etc.) and inhabitant
	 *    is a drawable block (not destroyable)
	 *  - Inhabitant is a destroyable character (minion/tower/building)
	 *    and tile contains only destroyable characters owned by
	 *    the same owner.
	 * @param inhabitant
	 * @return
	 */
	boolean pollInhabitant(Drawable inhabitant) {
		// TODO: Fixnut ostatne drawables ktore nemaju kolidovat s ostatnymi vecami
		if (null == owner()) {
			return true;
		}
		if (inhabitant instanceof Projectile) {
			return true;
		}
		if (inhabitant instanceof Destroyable) {
			if (inhabitants.isEmpty()) {
				return true;
			}
			if (owner() == ((Destroyable) inhabitant).owner) {
				return true;
			}
			return false;
		}
		return false;
	}

	/**
	 * Checks if tile contains a destroyable character.
	 * @return
	 */
	boolean isAttackable() {
		return (null != getTarget());
	}

	/**
	 * Returns owner of the tile (if any). Tile can contain a set
	 * of destroyable characters owned by the same player (owner),
	 * or a set of drawable blocks (walls), which are owned by no-one.
	 * @return
	 */
	Player owner() {
		Destroyable target = getTarget();
		if (null == target) {
			return null;
		} else {
			return target.owner;
		}
	}

	/**
	 * Creates the tile with new empty LinkedList of inhabitants.
	 */
	public Tile() {
		inhabitants = new LinkedList<Drawable>();
	}

	/**
	 * Creates the tile on the specified location.
	 * @param x
	 * @param y
	 */
	public Tile(int x, int y) {
		this();
		this.x = x;
		this.y = y;
	}
}
