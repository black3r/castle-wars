package com.matfyz.castlewars;

import com.badlogic.gdx.graphics.Texture;

/**
 * Class containing methods for towers (building capable of attack)
 *
 * Extends: Building(hyperlink)
 *
 * Extended by: there will be concrete towers
 */
public abstract class Tower extends Building {
	public Tower(Player owner, int x, int y, int tileSize, Texture sprite,
			int hp,
			int dmg, CastleWarsGame game) {
		super(owner, x, y, tileSize, sprite, hp, dmg, game);
	}

	public Tower(Player owner, int x, int y, int tileSize, Texture sprite,
			int hp, int dmg, CastleWarsGame game, long buildStep) {
		super(owner, x, y, tileSize, sprite, hp, dmg, game, buildStep);
	}

	@Override
	public void act() {
		// Try to find an enemy in sight range!
		Destroyable target =
				game.state.findNearestEnemyCloserThan(x, y, owner, sightRange);
		// in case anyone wonders: sightRange might be shorter than weaponRange sometimes in the future
		if (target == null) {
		} else {
			int range = game.state.distanceFrom(x, y, target);
			if (range <= weaponRange) {
				attack(target);
			}
		}
	}
}
